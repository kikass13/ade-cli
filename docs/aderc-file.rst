.. _aderc-file:

The ``.aderc`` File
^^^^^^^^^^^^^^^^^^^

As described in :ref:`ade-env-vars`, it is possible to configure ADE using environment
variables. In order to make these configurations project-wide, add the environment
variables to the ``.aderc``. At a minimum, the ``.aderc`` file should include the list
of images

.. literalinclude:: minimal-ade/.aderc
   :language: bash

Often, it will also need to include the Gitlab instance and registry, so ``ade`` knows
where to download images:

.. literalinclude:: example-aderc
   :language: bash
   :lines: 6-12

Lastly, it will also need to include some extra arguments to ``docker run`` to enable
debugging, for example:

.. literalinclude:: example-aderc
   :language: bash
   :lines: 1-2, 5-12

See :ref:`addargs` for more information.


.. _aderc-file-fork-pull:

Writing the ``.aderc`` file for pull+fork projects
""""""""""""""""""""""""""""""""""""""""""""""""""

The ``.aderc`` file supports using variables in the configuration file;
this feature can be leveraged to create an ``.aderc`` file for a project
that uses a fork+pull model.

In the example above, someone who forks the AutowareAuto project will
need to edit their ``.aderc`` file to pull the images from their fork's registry
instead of the AutowareAuto registry; this change will need to be reverted
before the fork gets pulled. The project could avoid this hassle by providing
the following instead:

.. literalinclude:: example-aderc-pull-fork
   :language: bash


If the ``AW_FORK_NAMESPACE`` variable is not set, the default ``autowareauto``
namespace will be used. Meanwhile, the developer of the fork can set ``AW_FORK_NAMESPACE``
in order to pull images from their own registry: e.g.::

   $ export AW_FORK_NAMESPACE=jpsamper
   $ ade start
   Starting ade with the following images:
   ade          | v0.1.0   | v0-1-0  | registry.gitlab.com/jpsamper/autowareauto/ade:v0-1-0
   autowareauto | v0.1.0   | v0-1-0  | registry.gitlab.com/jpsamper/autowareauto:v0-1-0
   atom         | v1.35.0  | latest  | registry.gitlab.com/apexai/ade-atom:latest

Furthermore, if they are working on ``my-branch`` instead of ``master``, they can use
the ``--select`` option:::

   $ export AW_FORK_NAMESPACE=jpsamper
   $ ade start --select my-branch
   Starting ade with the following images:
   ade          | <sha>    | my-branch  | registry.gitlab.com/jpsamper/autowareauto/ade:my-branch
   autowareauto | <sha>    | my-branch  | registry.gitlab.com/jpsamper/autowareauto:my-branch
   atom         | v1.35.0  | latest  | registry.gitlab.com/apexai/ade-atom:latest
