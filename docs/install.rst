.. _install:

Installation
------------

Requirements
^^^^^^^^^^^^
A Linux distribution with the following programs installed:
   * Docker 19.03 or newer, follow the `instructions for your Linux distribution`_
   * If the host machine has an NVIDIA graphics card, and the Docker version is older
     than 19.03, see the :ref:`nvidia-docker` article


Installation
^^^^^^^^^^^^

#. Verify that the Requirements above are fulfilled
#. Download the statically-linked binary from the `Releases`_ page of the ``ade-cli`` project
#. Name the binary ``ade`` and install it in your ``PATH``
#. Make the binary executable: ``chmod +x ade``
#. Check that it is installed:

.. code:: bash

   $ which ade
   /path/to/ade
   $ ade --version
   <version>


For information on how to install ADE (``ade-cli``, ADE base image, and ADE volumes)
on an offline machine, see :ref:`offline-install`.

.. toctree::
   :maxdepth: 4
   :hidden:

   offline-install


Update
^^^^^^

To update ``ade-cli``, run ``ade update-cli``. If a newer version is available,
``ade`` will prompt for confirmation, download the new version, and replace itself.


Autocompletion
^^^^^^^^^^^^^^

To enable autocompletion, add the following to your ``.zshrc`` or ``.bashrc``:

   .. literalinclude:: ../completion.sh
      :language: shell
      :lines: 3-


See :ref:`usage` for next steps.

.. _Releases: https://gitlab.com/ApexAI/ade-cli/-/releases
.. _instructions for your Linux distribution: https://docs.docker.com/install/#server
